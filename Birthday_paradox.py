import random

times = int(input('Please enter the number of trials: '))
class_size = int(input('Please enter the class size: '))
total_repeat_count = 0

for time in range(times):
    birthday_dict = {}

    for student in range(class_size):
        birthday = random.randint(1, 365)
        birthday_dict[birthday] = birthday_dict.get(birthday, 0) + 1

    repeat_count = 0 

    for k, v in birthday_dict.items():
        if v > 1:
            repeat_count = 1
    total_repeat_count += repeat_count

probability = total_repeat_count/times
print(probability)
            

