# C: Finding the right amount to save away (bisection search​)
def current_savings(annual_salary, portion_saved, semi_annual_raise, invest_return, goal_month):
    current_savings = 0
    monthly_savings = annual_salary/12*portion_saved
    month = 0
    while month < goal_month:
        for i in range(6):
            current_savings = (monthly_savings + current_savings + current_savings*invest_return/12)
            month += 1
        annual_salary = annual_salary * (1 + semi_annual_raise)
        monthly_savings = annual_salary/12*portion_saved
    return current_savings

annual_salary = int(input('Enter your starting annual salary:'))
total_cost = 1000000
semi_annual_raise = 0.07
portion_down_payment = 0.25
invest_return = 0.04
epsilon = 100
goal_month = 36

numSearch = 0
low = 0.0
high = 1.0
portion_saved = (low + high)/2

if current_savings(annual_salary, 1, semi_annual_raise, invest_return, goal_month) < total_cost*portion_down_payment:
    print('It is not possible to pay the down payment in three years.')

else:
    while abs(current_savings(annual_salary, portion_saved, semi_annual_raise, invest_return, goal_month) - total_cost*portion_down_payment) >= 100:
        numSearch += 1
        if current_savings(annual_salary, portion_saved, semi_annual_raise, invest_return, goal_month) > total_cost*portion_down_payment:
            high = portion_saved
        else:
            low = portion_saved
        portion_saved = (low + high)/2
    print('Best saving rate is:', round(portion_saved, 4))
    print('numSearch =', numSearch)
