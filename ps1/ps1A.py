# A: Housing hunt
annual_salary = int(input('Enter your annual salary:'))
portion_saved = float(input('Enter the percent of your salary to save, as a decimal:'))
total_cost = int(input('Enter the cost of your dream home:'))


portion_down_payment = 0.25
current_savings = 0
monthly_savings = annual_salary/12*portion_saved
r = 0.04
month = 0

while current_savings < (total_cost * portion_down_payment):
    current_savings = (monthly_savings + current_savings + current_savings*r/12)
    month += 1

print('Number of months:', month)
