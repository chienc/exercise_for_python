#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 00:21:06 2018

@author: chienc
"""

#3.3
total = 0
for c in [1.23,2.4,3.123]:
    total = total + float(c)
print(total)

#3.4-1
x = 25
epsilon = 0.01
numGuess = 0
low = 0.0
high = max(1.0, x)
ans = (high + low)/2.0
while abs(ans**2-x) >= epsilon:
    print('low =', low, 'high =', high, 'ans =', ans)
    numGuess += 1
    if ans**2 < x:
        low = ans
    else:
        high = ans
    ans = (high + low)/2.0
print('numGuess =', numGuess)
print(ans, 'is close to square root of x')

#3.4-2
x = -27
epsilon = 0.01
numGuess = 0
low = 0.0
high = max(1.0, abs(x))
ans = (high + low)/2.0
while abs(ans**3-abs(x)) >= epsilon:
    print('low =', low, 'high =', high, 'ans =', ans)
    numGuess += 1
    if ans**3 < abs(x):
        low = ans
    else:
        high = ans
    ans = (high + low)/2.0
print('numGuess =', numGuess)
if x>0:
    print(ans, 'is close to square root of x')
else:
    print(-ans, 'is close to square root of x')
