#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 14:09:04 2018

@author: chienc
"""

#2.4-1
numXs=int(input('How many times should I print the letter X?'))
toPrint=''
#concatenate X to toPrint numXs times
while numXs>0:
    toPrint='X'*numXs
    print(toPrint)
    break
while numXs>0:
    print('X')
    numXs=numXs-1
    
#2.4-2
n1=int(input('Enter the first number:'))
n2=int(input('Enter the second number:'))
n3=int(input('Enter the third number:'))
n4=int(input('Enter the forth number:'))
n5=int(input('Enter the fifth number:'))
n6=int(input('Enter the sixth number:'))
n7=int(input('Enter the seventh number:'))
n8=int(input('Enter the eighth number:'))
n9=int(input('Enter the ninth number:'))
n10=int(input('Enter the tenth number:'))

numbers=[n1,n2,n3,n4,n5,n6,n7,n8,n9,n10]
odd_nums=[]
index=0

for number in numbers:
    if number%2 != 0:
        odd_nums.append(number)
    
if odd_nums == []:
    print('None of them are odd.')
if odd_nums != []:
    largest_odd=odd_nums[0]
    for odd_num in odd_nums:
        if odd_num > largest_odd:
            largest_odd = odd_num

print(str(largest_odd) + ' is the largest odd.')
        