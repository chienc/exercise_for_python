# Python exercise
## Exercise from Introduction to Computing and Programming Using Python
Finger exercise in chapter 1 - 10
## MIT Problemset 0-5
0. Raising a number to a power and taking a logarithm
1. House hunting
    Part A: Housing hunt
    Part B: Saving, with a raise
    Part C: Finding the right amount to save away (bisection search​)
2. Hangman
3. Word Game
4. Encryption and Decryption
    Part A: Permutations of a string
    Part B: Cipher Like Caesar
    Part C: Substitution Cipher
5. RSS feed

## Euler project
1. Multiples of 3 and 5
2. Even Fibonacci Numbers
3. Largest prime factor
4. Largest palindrome product

## Other exercise
- Monty Hall
- 100 Prisoners and light bulb
- Birthday paradox
- Guess a number game (AB game)
