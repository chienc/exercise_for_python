import random
chosen_nums = random.sample(range(0, 10), 4)

numstr = ''.join(map(str, chosen_nums))

guess = 0

while True:
    guess += 1
    ans = str(input('Please enter your answer: '))
    if ans == numstr:
        print('Congratulations! You win with ' + str(guess) + 'guess!')
        break
    else:
        A = 0
        B = 0
        for i in ans:
            if i in numstr and ans.find(i) == numstr.find(i):
                A += 1
            elif i in numstr and ans.find(i) != numstr.find(i):
                B += 1
        print(str(guess) + 'guess:' + str(A) + 'A' + str(B) + 'B')
