#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 29 13:43:47 2018

@author: chienc
"""
#3.1
x=int(input('Please enter an integer:'))

root = 0
pwrs = [1,2,3,4,5,6]
pairs=[]

if x>0:
    while root < abs(x):
        for pwr in pwrs:
            if root**pwr == x:
                print('Root:', root, ', ', 'Power:', pwr)
                pairs.append((root,pwr))
        root = root + 1
    
if x<0:
    root = 0
    while root > x:
        for pwr in pwrs:
            if root**pwr == x:
                print('Root:', root, ', ', 'Power:', pwr)
                pairs.append((root,pwr))
        root = root - 1

else:
    if pairs == []:
        print('No such pair of integers exist.')


