def findAnEven(L):
    """Assume L is a list of integers
        Return the first even number in L
        Raises ValueError if L does not contain an even number"""
    finding = 1
    for num in L:
        try:
            if int(num)%2 == 0:
                finding = num
                break
        except ValueError:
            continue
    if finding != 1:
        return finding
    else:
        raise ValueError('There\'s no even number in L.')

try:
    print(findAnEven([1,2,3,4,5]))
    print(findAnEven([1,3,5,999]))
    print(findAnEven(['a','b','c']))
except ValueError as msg:
    print(msg)
