#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 09:13:09 2018

@author: chienc
"""
import numpy as np

def hangman(number_of_guess):
    openfile = open('words.txt', 'r')
    words = openfile.read().split()
    word_chosen = np.random.choice(words)
    print("_ "*len(word_chosen))
    word_guess = list("_ "*len(word_chosen))
    
    for n in range(1, number_of_guess + 1):
        guess_al = input("Please enter an alphabet or make a guess for the word: ")
        if len(guess_al) == 1:
            if n < number_of_guess:
                for al_num in range(len(word_chosen)):
                    if word_chosen[al_num] == guess_al:
                        word_guess[2*al_num] = guess_al
                print(''.join(word_guess))
                print("Number of guess left:", number_of_guess-n)
            elif n == number_of_guess:
                ans = input("Please make a guess for the word:")
                if ans == word_chosen:
                    print("Bingo!")
                else:
                    print("Dead!")
        else:
            if guess_al == word_chosen:
                print("Bingo!")
                break
            else:
                print("Wrong!")
                print("Number of guess left:", number_of_guess-n)
                continue


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    openfile = open('words.txt', 'r')
    words = openfile.read().split()
    secret_word = np.random.choice(words)
    letters_guessed = []
    for w in secret_word:
        if w in letters_guessed == True:
            
            continue
        return True
        

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    