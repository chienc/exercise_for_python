#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 14:09:04 2018

@author: chienc
"""
#2.2
x=input('x=')
y=input('y=')
z=input('z=')

x=int(x)
y=int(y)
z=int(z)

if x%2 != 0 and y%2 != 0 and z%2 != 0:
    if x>y and x>z:
        print('x is the largest odd.')
    elif y>z:
        print('y is the largest odd.')
    else:
        print('z is the largest odd.')
elif x%2 != 0 and y%2 != 0 and z%2 == 0:
    if x>y:
        print('x is the largest odd.')
    else:
        print('y is the largest odd.')
elif x%2 != 0 and y%2 == 0 and z%2 != 0:
    if x>z:
        print('x is the largest odd.')
    else:
        print('z is the largest odd.')
elif x%2 == 0 and y%2 != 0 and z%2 != 0:
    if y>z:
        print('y is the largest odd.')
    else:
        print('z is the largest odd.')
elif x%2 != 0 and y%2 == 0 and z%2 == 0:
    print('x is the largest odd.')
elif x%2 == 0 and y%2 != 0 and z%2 == 0:
    print('y is the largest odd.')
elif x%2 == 0 and y%2 == 0 and z%2 != 0:
    print('z is the largest odd.')
else:
    print('None of them are odd.')