# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''
    result = []
    if len(sequence) == 1:
        return sequence
    else:
        for i in get_permutations(sequence[1:]):
            for j in range(len(i)+1):
                s = i[:j] + sequence[0] + i[j:]
                result.append(s)
    return result

if __name__ == '__main__':
#    #EXAMPLE
   example_input1 = 'abc'
   print('Input1:', example_input1)
   print('Expected Output1:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
   print('Actual Output1:', get_permutations(example_input1))

   example_input2 = '123'
   print('Input2:', example_input2)
   print('Expected Output2:', ['123', '132', '213', '231', '312', '321'])
   print('Actual Output2:', get_permutations(example_input2))

   example_input3 = '87'
   print('Input3:', example_input3)
   print('Expected Output3:', ['87', '78'])
   print('Actual Output3:', get_permutations(example_input3))

#    # Put three example test cases here (for your sanity, limit your inputs
#    to be three characters or fewer as you will have n! permutations for a
#    sequence of length n)
