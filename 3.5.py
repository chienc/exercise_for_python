#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 01:06:22 2018

@author: chienc
"""

#3.5
#Newton-Raphson for square root
k=int(input('Please enter a number:'))
epsilon = 0.01
guess = k/2.0
while abs(guess*guess - k) >= epsilon:
    guess = guess - (((guess**2) - k)/(2*guess))
print('Square root of', k, 'is about', guess)