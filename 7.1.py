def sumDigits(s):
    """Assume s is a string
        Returns the sum of the decimal digits in s
            For example, if s is 'a2b3c' it returns 5"""
    if type(s) == str:
        sum = 0
        for i in s:
            try:
                num = int(i)
            except ValueError:
                num = 0
            sum += num
        print(sum)
    else:
        print('The input should be a string.')
