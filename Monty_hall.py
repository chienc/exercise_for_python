import random

def door_with_prize(doors):
    right_door = random.randint(1,doors+1) 
    return right_door

def get_the_right_door(right_door, choice):
    if right_door == choice:
        return True
    return False

def result_of_change(doors, first_choice):
    right_door = door_with_prize(doors)
    if right_door == first_choice:
        return False
    else:
        return True

def test_change():
    iteration = int(input('How many iterations?'))
    doors = int(input('How many doors are there?'))
    first_choice = int(input('Please choose a door: '))
    count_win = 0
    for i in range(iteration):
        result_of_change(doors, first_choice)
        if result_of_change(doors, first_choice) == True:
            count_win += 1
    win = count_win/iteration
    lose = 1 - win
    print(win, 'of chance winning if you switch.')
    print(lose, 'of chance winning if you don\'t switch')

test_change()