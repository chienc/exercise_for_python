#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 13:41:37 2018

@author: chienc
"""

#If we list all the natural numbers below 10 that are multiples of 3 or 5, 
#we get 3, 5, 6 and 9. The sum of these multiples is 23.
#Find the sum of all the multiples of 3 or 5 below 1000.

sums = 0

for num in range(1000):
    if num%3 == 0 or num%5 == 0:
        sums = sums + num
print(sums)
        
        