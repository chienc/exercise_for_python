# A palindromic number reads the same both ways.
# The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 * 99.
# Find the largest palindrome made from the product of two 3-digit numbers.


def largest_palindrome_number(n):
    
    def isPal(s):
        if len(s) <= 1:
            return True
        else:
            return s[0] == s[-1] and isPal(s[1: -1])
    
    lst = []
    for num1 in range(10**n-1, 10**(n-1)-1, -1):
        for num2 in range(10**n-1, 10**(n-1)-1, -1):
            ans = num1 * num2
            if isPal(str(ans)):
                lst.append(ans)
    print(lst[0])
                